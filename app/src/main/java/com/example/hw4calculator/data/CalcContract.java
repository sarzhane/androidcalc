package com.example.hw4calculator.data;

public interface CalcContract {
    void init(CallBack callBack);

    void listener(String val);


    interface CallBack{
        void result(String valRes);


        void clear();
    }
}
