package com.example.hw4calculator.app;

import android.app.Application;

import com.example.hw4calculator.BuildConfig;

import timber.log.Timber;

public class App extends Application {

    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

    }
}
