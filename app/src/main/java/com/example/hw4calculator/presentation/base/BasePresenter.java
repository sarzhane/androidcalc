package com.example.hw4calculator.presentation.base;

public interface BasePresenter<V> {
    void startView(V view);

    void detachView();
}
