package com.example.hw4calculator.presentation;

import com.example.hw4calculator.presentation.base.BasePresenter;

public interface MainContract {
    interface View {
        void showDisplay(String val);

        void showResult(String val);
    }

    interface Presenter extends BasePresenter<View> {
        void eventOperation(String val);
    }
}
