package com.example.hw4calculator.presentation;

import com.example.hw4calculator.R;
import com.example.hw4calculator.databinding.ActivityMainBinding;
import com.example.hw4calculator.presentation.base.BaseActivity;
import com.example.hw4calculator.presentation.base.BasePresenter;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
    private MainContract.Presenter presenter;


    @Override
    public void showDisplay(String val) {
        getBinding().inputText.setText(val);

    }

    @Override
    public void showResult(String val) {
        getBinding().resultText.setText(val);
    }


    @Override
    public void toast(String val) {

    }



    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new MainPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }
}


